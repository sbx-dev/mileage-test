import axios from 'axios';
import urlencode from 'urlencode';
import {publicEncRSA} from "../../utils";

export default async (req, res) => {
  if(req.method !== 'POST' || !req.body) {res.status(404).end();return;}

  const {
    token : {
      token_type,
      access_token
    },
    code,
    id,
    pw
  } = req.body;

  const {data} = await axios({
    // url : 'https://api.codef.io/v1/kr/etc/tr/airline/mileage-list',
    url : 'https://development.codef.io/v1/kr/etc/tr/airline/mileage-list',
    method : 'post',
    headers : {
      'Content-Type': 'application/json',
      'Authorization': `${token_type} ${access_token}`
    },
    data : urlencode.encode(JSON.stringify({
      organization : code,
      type : '0',
      userId : id,
      userPassword : publicEncRSA(process.env.PUBLIC_KEY, pw)
    }))
  })

  res.status(200).json(urlencode.decode(data));
}
