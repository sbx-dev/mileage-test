import App from 'next/app'
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';


const MyApp = ({ Component, pageProps }) => (
  <Component {...pageProps} />
);

MyApp.getInitialProps = async (appContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const {pageProps} = await App.getInitialProps(appContext);
  const client_id = process.env.CLIENT_ID;
  const client_secret = process.env.CLIENT_SECRET;

  const authHeader = new Buffer(`${client_id}:${client_secret}`).toString('base64');

  const {data} = await axios({
    url : 'https://oauth.codef.io/oauth/token',
    method : 'post',
    headers : {
      'Acceppt': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': `Basic ${authHeader}`
    },
    params : {
      'grant_type' :  'client_credentials',
      'scope' : 'read',
    }
  });

  return {
    pageProps : {
      ...pageProps,
      ...data
    }
  }
}

export default MyApp