import crypto from 'crypto';
import constants from 'constants';

const publicEncRSA = (publicKey, data) => {
  const pubkeyStr = `-----BEGIN PUBLIC KEY-----\n${publicKey}\n-----END PUBLIC KEY-----`;
  const bufferToEncrypt = new Buffer(data);
  return crypto.publicEncrypt({"key" : pubkeyStr, padding : constants.RSA_PKCS1_PADDING},bufferToEncrypt).toString("base64");
}

export {
  publicEncRSA
}