import React, {useState} from 'react';
import {Row, Col, Card, Form, Button, Table, Spinner} from "react-bootstrap";
import axios from 'axios';
import moment from 'moment';

const Miles = ({defaultId, defaultPw, title, code, token}) => {
  const [id, setId] = useState(defaultId);
  const [pw, setPw] = useState(defaultPw);
  const [loading, setLoading] = useState(false);
  const [data, setData]= useState(undefined);
  const [timer, setTimer] = useState(0);

  const onChangeId = ({target : {value}}) => {setId(value)}
  const onChangePw = ({target : {value}}) => {setPw(value)}
  const onClick = async () => {
    setLoading(!loading);

    const start = moment();
    const {data} = await axios({
      url : '/api/getMiles',
      method : 'post',
      data : {token, code, id, pw}
    })
    const finish = moment();

    setLoading(!loading);
    setTimer(finish.diff(start, 'seconds'));
    setData(data);
  };

  const empty = () => (
    <Card>
      <Card.Header>{title} Ready</Card.Header>
      <Card.Body className="text-center">Empty</Card.Body>
    </Card>
  );
  const loader = () => (
    <Card>
      <Card.Header>{title}  Loading</Card.Header>
      <Card.Body className="text-center">
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      </Card.Body>
    </Card>
  )

  const render = () => {
    const {
      result : {
        message,
        extraMessage
      },
      data : {
        commUniqueNo,
        resEarnedMileage,
        resUsedMileage,
        resExpiredMileage,
        resRemainingMileage,
        resDetailList = []
      }
    } = data;
    return (
      <Card>
        <Card.Header>{title} Result : {message.replace(/\+/g, ' ')} {extraMessage}</Card.Header>
          <Card.Body>
            <Card.Title>마일리지 데이터</Card.Title>
            <Table striped bordered hover size="sm">
              <tbody>
                <tr>
                  <td>고유번호</td>
                  <td>{commUniqueNo}</td>
                </tr>
                <tr>
                  <td>적립 마일리지</td>
                  <td>{resEarnedMileage}</td>
                </tr>
                <tr>
                  <td>사용 마일리지</td>
                  <td>{resUsedMileage}</td>
                </tr>
                <tr>
                  <td>소멸 마일리지</td>
                  <td>{resExpiredMileage}</td>
                </tr>
                <tr>
                  <td>잔여 마일리지</td>
                  <td>{resRemainingMileage}</td>
                </tr>
              </tbody>
            </Table>
            <Card.Text>마일리지 상세내역</Card.Text>
            <Table bordered hover size="sm">
              <thead>
                <tr>
                  <td>마일리지</td>
                  <td>만료일</td>
                </tr>
              </thead>
              <tbody>
                {
                  resDetailList.map(({resMileage, resExpirationDate}, i) => (
                    <tr key={i}>
                      <td>{resMileage}</td>
                      <td>{resExpirationDate}</td>
                    </tr>
                  ))
                }
              </tbody>
            </Table>
          </Card.Body>
        <Card.Footer>
          걸린 시간 : {timer} seconds
        </Card.Footer>
      </Card>
    )
  }

  return (
    <Row className="mb-3">
      <Col md={4}>
        <Card>
          <Card.Header>{title}</Card.Header>
          <Card.Body>
            <Form>
              <Form.Group controlId={`${code}id`}>
                <Form.Label>ID</Form.Label>
                <Form.Control placeholder="Enter ID" onChange={onChangeId} defaultValue={id}/>
              </Form.Group>
              <Form.Group controlId={`${code}pw`}>
                <Form.Label>PASSWORD</Form.Label>
                <Form.Control type="password" placeholder="Enter PASSWORD" onChange={onChangePw} defaultValue={pw}/>
              </Form.Group>
            </Form>
            <Button block variant="outline-secondary" onClick={onClick}>login</Button>
          </Card.Body>
        </Card>
      </Col>
      <Col>
        {
          !data && !loading
            ? empty()
            : !data && loading
            ? loader()
            : render()
        }
      </Col>
    </Row>
  )
}

export default Miles;